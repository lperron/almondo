/*
 * cs43l22.hpp
 *
 *  Created on: Mar 30, 2020
 *      Author: lperron
 */
#ifndef CS43L22_HPP
# define CS43L22_HPP
#include "stm32f4xx.h"
#ifdef USE_MIDI_USB
 #define DMA_BUFFER_SIZE 256
 #define DMA_BUFFER_HALF_SIZE 128
#else
 #define DMA_BUFFER_SIZE 128
 #define DMA_BUFFER_HALF_SIZE 64
#endif
#define DMA_NOP 0
#define DMA_HT 1
#define DMA_TC 2

#define VERIFY_WRITTENDATA

extern int16_t _DMA_Buffer[DMA_BUFFER_SIZE] __attribute__((section(".ram")));

class cs43l22
{
	I2C_TypeDef *_I2Cx;
	uint8_t _addr;
public :
	cs43l22();
	void init();
	uint32_t set_volume(uint8_t Volume);
	uint32_t init_registers(void);
	uint32_t write_register(uint8_t RegisterAddr, uint8_t RegisterValue);
	uint32_t read_register(uint8_t RegisterAddr);
};

#endif
