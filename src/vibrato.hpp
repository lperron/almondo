/*
 * vibrato.hpp
 *
 *  Created on: Apr 18, 2020
 *      Author: lperron
 */

#ifndef VIBRATO_HPP_
#define VIBRATO_HPP_
#include "addit.hpp"
#define MAX_DELAY_SAMPLES 256//please keep a power of two here
#define LFO_BUFFER_SIZE 256 //as U want

class vibrato{
private :
	int16_t _delay_line[MAX_DELAY_SAMPLES]; //ciruclar buffer
	float _lfo_buffer[LFO_BUFFER_SIZE];
	float _lfo_head_index;
	uint16_t _width; //must be < to MAX_DELAY_SAMPLES
	uint16_t _head;
	float _freq;
	float _lfo_incr;
public :
	vibrato()
	{
		int i;

		_width = 0;
		_head = 0;
		_freq = 1;
		_lfo_incr = 0;
		i = -1;
		while (++i < MAX_DELAY_SAMPLES)
			_delay_line[i] = 0;
		i = -1;
		while (++i < LFO_BUFFER_SIZE)
			_lfo_buffer[i] = (sinf(2 * M_PI * i / (float) LFO_BUFFER_SIZE) + 1.) / 2.;
		_lfo_head_index = 0;
	};

	void	set_width(float width) //there's a problem with this method : we can stop the lfo and keep a delay of prev width...
	//like if we have a width of 1 and then turn it to 0 we'll keep a delay of 1024 samples...
	{
		if (width < 0 || width > 1)
			return;
		_width = width * MAX_DELAY_SAMPLES;
		_lfo_incr = LFO_BUFFER_SIZE * _freq / (float) SAMPLE_RATE;
	}

	void	set_freq(float freq)
	{
		if (freq < 0 || freq > 200)
			return ;
		_freq = freq;
		_lfo_incr = LFO_BUFFER_SIZE * freq / (float) SAMPLE_RATE;
	}

	float get_lfo_value()
	{
		float ret;
		int16_t flr;
		float fract;

		_lfo_head_index += _lfo_incr;
		if (_lfo_head_index >= LFO_BUFFER_SIZE)
			_lfo_head_index -= LFO_BUFFER_SIZE;
		flr = (int16_t)floorf(_lfo_head_index);
		fract = _lfo_head_index - flr;
		ret = _lfo_buffer[flr] *(1 - fract);
		flr++;
		if (flr >= LFO_BUFFER_SIZE)
			flr -= LFO_BUFFER_SIZE;
		ret += fract *_lfo_buffer[flr];
		return (ret * _width);
	}

	int16_t vibrato_effect(int16_t value)
	{
		int16_t ret;
		int16_t ret_index;
		float tmp;
		float floortmp;
		//write
		_delay_line[_head] = value;
		//update offset
		tmp = get_lfo_value();
		floortmp = floorf(tmp);
		tmp -= floortmp;
		ret_index = _head - (int16_t)floortmp;
		if (ret_index < 0)
			ret_index += MAX_DELAY_SAMPLES;
		//read
		ret = _delay_line[ret_index];
		ret_index--;
		if (ret_index < 0)
			ret_index += MAX_DELAY_SAMPLES;
		ret = ret * (1 - tmp) + tmp * _delay_line[ret_index];
		_head = (_head + 1) & (MAX_DELAY_SAMPLES - 1);//fast power of to modulo*
		return (ret);
	};
};




#endif /* VIBRATO_HPP_ */
