/*
 * time.cpp
 *
 *  Created on: Mar 26, 2020
 *      Author: lperron
 */


#include "stm32f4xx.h"

volatile uint32_t ticks;

extern "C" void SysTick_Handler(void)
 {
   ticks++;
 }

void systickInit(uint16_t frequency)
{
   RCC_ClocksTypeDef RCC_Clocks;
   RCC_GetClocksFreq (&RCC_Clocks);
   (void) SysTick_Config (RCC_Clocks.HCLK_Frequency / frequency);
}

void	systickInit_ms(void)
{
	systickInit(48000);
}

uint32_t millis()
{
	return (ticks);
}

void delay_ms (uint32_t t)
{
  uint32_t start, end;

  start = millis();
  end = start + t;
  if (start < end)
  {
  	while ((millis() >= start) && (millis() < end))
  	  ;
  }
  else
  {
    while ((millis() >= start) || (millis() < end))
      ;
  }
}
