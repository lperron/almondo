/*
 * midi.hpp
 *
 *  Created on: Mar 27, 2020
 *      Author: lperron
 */

#ifndef MIDI_HPP_
#define MIDI_HPP_

#include "midihandler.hpp"
#include "circularBuffer.hpp"

#ifdef USER_MIDI_IN_CHANNEL
# define MIDI_IN_CHANNEL USER_MIDI_IN_CHANNEL - 1
#else
# define MIDI_IN_CHANNEL 0
#endif

//size three
#define NOTEOFF 0x8 << 4
#define NOTEON 0x9 << 4
#define POLYPHONIC_AFTERTOUCH 0xA << 4
#define CONTROL_CHANGE 0xB << 4
#define PITCHBEND 0xE << 4
#define SONG_POSITION 0xF2

//size two
#define PROGRAM_CHANGE 0xC << 4
#define CHANNEL_AFTERTOUCH 0xD << 4
#define QUARTER_FRAME 0xF1
#define SONG_SELECT 0xF3

//size one
#define RESERVED1 0xF4
#define RESERVED2 0xF5
#define TUNE_REQUEST 0xF6
#define CLOCK 0xF8
#define RESERVED3 0xF9
#define START 0xFA
#define CONTINUE 0xFB
#define STOP 0xFC
#define RESERVED4 0xF5
#define SENSING 0xFE
#define RESET 0xFF


//sysex
#define SYSEX 0xF0
#define SYSEX_END 0xF7

#define OMNI 0xF

class Midi {
private :
	circularBuffer _midiInBuff;
	unsigned char _message[3];
	unsigned char _running;
	unsigned char _third;
	unsigned char _channel;
public :
	Midi();
	void read();
	void begin(unsigned char channel);
	void sysex_parser(unsigned char data);
	void status_parser(unsigned char data);
	void data_parser(unsigned char data);
	void two_bytes_mes(unsigned char data);
	void three_bytes_mes(unsigned char data);
	void	handle_realtime(unsigned char messa);
	void	handle_message(unsigned char *message);
	void putDataInMidiInBuffer(unsigned char data);
};


#endif /* MIDI_HPP_ */
