/*
 * addit.hpp
 *
 *  Created on: Apr 3, 2020
 *      Author: lperron
 */

#ifndef ADDIT_HPP_
#define ADDIT_HPP_
#define SAMPLE_RATE 48000 //DO NOT CHANGE, or change also the value in cs43l22
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "stm32f4xx.h"
#include "stmtime.h"
#include "circularBuffer.hpp"
#include "cs43l22.hpp"
#include "vibrato.hpp"
#include "midiControls.h"


enum note_state { STATE_OFF, STATE_ON, STATE_SUS, STATE_SOSP, STATE_SOSR }; //sosp == sostenuto + pressed  sosr = sostenuto + released
/////////NEW_STUFF/////////////////
#define NB_TONE_WHEELS 109
#define NB_DRAWBARS 9
#define MIN_NOTE 36
#define MAX_NOTE 97

/////////EDITABLE/////////////////////////////////////////////
#define RND_PHASE
#define HAMMOND_TEMP
#define LEAKS_ON
#ifdef USE_MIDI_USB
#define NB_VOICES 18
#define NB_VOICES_LEAKS 12
#else
#define NB_VOICES 20
#define NB_VOICES_LEAKS 13
#endif

#define SAMPLES_COUNT_BEFORE_ENV_UPDATE 8
#define FSAMPLES_COUNT_BEFORE_ENV_UPDATE 8.f

///////////////////////////////////////////////////////////////

struct zero_sine_buffer
{
	const float *buffer;
	uint16_t size;
};
typedef struct zero_sine_buffer t_zero_sine_buffer;

struct tone_wheels
{
	const float *buffer;
	tone_wheels *leaking;
	const float *end;
	const float *buffer_ref;
	uint16_t size;
	uint16_t speed;
};
typedef struct tone_wheels t_tone_wheels;


struct voice
{
	t_tone_wheels *harmonics[NB_DRAWBARS];
	float env;
	float envIncr;
	float envMin;
	uint32_t timeoff;
	uint8_t	gate;
	uint8_t key;
};

typedef struct voice t_voice;
float get_freq(char note);

uint16_t get_buff_size(float freq);

t_zero_sine_buffer	get_buffer(char note);

t_zero_sine_buffer	*create_all_sines();

void	free_all_sines(t_zero_sine_buffer *tofree);

float hardclip(float value);

//float softclip(float value); //NOT ANYMORE (works with values between -1 n 1)

void set_voice_tone(int note, int8_t *tone_number);


class addit
{
private :
	t_tone_wheels _tone_wheels[NB_TONE_WHEELS]; //once per sample
	t_voice _voices[NB_VOICES];
	float _HARlvlCorrected[NB_DRAWBARS];//one access per DRAW per voice per sample/2
	float _HARlvl[NB_DRAWBARS]; //only accessed when updating drawbar or lvl
	vibrato _vbr[1]; //stack it!!
	float value;//one per voice per sample/2
	float _destroy_factor; //1 per sample
	float _vibrato_factor;
	float _lvl;
	float _attack;
	float _decay;
	float _sustain;
	float _release;
	float _leaks;
	uint8_t _nb_voices;
	uint8_t _leaks_on;
	uint8_t _decayMidiValue;
	uint8_t _sustained; //rare
	uint8_t _sostenuto; //rare
	t_zero_sine_buffer *_sines;//almost never
public :
	addit();
	void getEnvs();
	void turn_wheels();
	void init_wheels();
	void setHARlvl(unsigned char val, unsigned char id);
	void setAttack(unsigned char val);
	void setDecay(unsigned char val);
	void setSustain(unsigned char val);
	void setLvl(unsigned char val);
	void setDestr(unsigned char val);
	void setVibrato(unsigned char val);
	void setRelease(unsigned char val);
	void setSustainOn(unsigned char val);
	void setSostenutoOn(unsigned char val);
	void setLeaksOn(unsigned char val);
	void setLeaksLvl(unsigned char val);
	void noteon(char note, char vel);
	void noteoff(char note);
	void set_voice_tone(int note, t_tone_wheels **harmonics);
	void allNoteOff();
	void panic();
	void switch_leaks();
	void fillBuff();

	~addit();
};

#endif /* ADDIT_HPP_ */
