/*
 * GPIOPin.h
 *
 *  Created on: Mar 24, 2020
 *      Author: lperron
 */

#ifndef GPIOPIN_H_
#define GPIOPIN_H_
# include "stm32f4xx.h"

class GPIOPin {
private :
	GPIO_TypeDef *_GPIOx;
	uint32_t _pin;
	GPIOMode_TypeDef _mode;
	GPIOOType_TypeDef _type;
	GPIOPuPd_TypeDef _pupd;
	GPIOSpeed_TypeDef _speed;
public:
	GPIOPin();
	void init(GPIO_TypeDef *GPIOx, uint32_t pin, GPIOMode_TypeDef mode =  GPIO_Mode_OUT, GPIOOType_TypeDef type = GPIO_OType_PP, GPIOPuPd_TypeDef pupd = GPIO_PuPd_NOPULL,GPIOSpeed_TypeDef speed = GPIO_Speed_50MHz);
	void digitalWrite( BitAction act);
	char digitalRead();
	virtual ~GPIOPin();
};

#endif /* GPIOPIN_H_ */
