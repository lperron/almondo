/*
 * circularBuffer.hpp
 *
 *  Created on: Mar 29, 2020
 *      Author: lperron
 */

#ifndef CIRCULAR_BUFFER_
# define CIRCULAR_BUFFER_

#include "stm32f4xx.h"

#define CIRCULAR_BUFFER_SIZE 64 //must be a power of 2

////////////INTERRUPT SAFE///////////////
class circularBuffer
{
private :
	volatile uint8_t _byte[CIRCULAR_BUFFER_SIZE];
	volatile uint32_t _head;
	volatile uint32_t _tail;

public :
	circularBuffer()
	{
		int i;

		i = -1;
		_head = 0;
		_tail = 0;
		while (++i < CIRCULAR_BUFFER_SIZE)
			_byte[i] = 0;
	}

	uint16_t size()
	{
		return (CIRCULAR_BUFFER_SIZE);
	}

	uint8_t readable()
	{
		return (_head != _tail);// we can read
	}

	uint8_t read() //not so good, you should always use readable ? fastRead
	{
		uint8_t ret;

	    if (_head != _tail)
	    {
	        ret = _byte[_tail];
	        _tail = (_tail + 1) & (CIRCULAR_BUFFER_SIZE - 1);
	        return (ret);
	    }
	    return (0); //we don't know if it's an error, please use readable ? fastRead
	}

	uint8_t fastRead()
	{
		uint8_t ret;

	    ret = _byte[_tail];
	    _tail = (_tail + 1) & (CIRCULAR_BUFFER_SIZE - 1);
	    return (ret);
	}

	uint8_t writable()
	{
		return (((_head + 1) & (CIRCULAR_BUFFER_SIZE - 1)) != _tail);
	}

	uint8_t	write(uint8_t data)
	{
		uint32_t next;
	    next = (_head + 1) & (CIRCULAR_BUFFER_SIZE - 1);
	    if (next != _tail)
	    {
	        _byte[_head] = data;
	        _head = next;
	        return (1);
	    }
	    return (0);
	}

	void forcedWrite(uint8_t data)
	{
		uint32_t next;
	    next = (_head + 1) & (CIRCULAR_BUFFER_SIZE - 1);
	    _byte[_head] = data;
	    _head = next;
	}
};

#endif
