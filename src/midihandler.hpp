/*
 * midihandler.hpp
 *
 *  Created on: Mar 28, 2020
 *      Author: lperron
 */

#ifndef MIDIHANDLER_HPP_
#define MIDIHANDLER_HPP_

__attribute__((weak)) void midiNoteOnHandler(unsigned char channel, unsigned char note, unsigned char vel);
__attribute__((weak)) void midiNoteOffHandler(unsigned char channel, unsigned char note, unsigned char vel);
__attribute__((weak)) void midiControlChangeHandler(unsigned char channel, unsigned char param, unsigned char value);
__attribute__((weak)) void midiPolyphonicAftertouchHandler(unsigned char channel, unsigned char note, unsigned char value);
__attribute__((weak)) void midiPitchbendHandler(unsigned char channel, uint16_t value); //14bit value
__attribute__((weak)) void midiSongPositionHandler(uint16_t position); //14bit beats (6clocks) elapsed
__attribute__((weak)) void midiProgramChangeHandler(unsigned char channel, unsigned char program);
__attribute__((weak)) void midiChannelAftertouchHandler(unsigned char channel, unsigned char value);
__attribute__((weak)) void midiQuarterFrameHandler(unsigned char value); //0NNNDDDD NNN=message DDDD=value
__attribute__((weak)) void midiSongSelectHandler(unsigned char song);
__attribute__((weak)) void midiTuneRequestHandler(void);
__attribute__((weak)) void midiClockHandler(void);
__attribute__((weak)) void midiStartHandler(void);
__attribute__((weak)) void midiContinueHandler(void);
__attribute__((weak)) void midiStopHandler(void);
__attribute__((weak)) void midiSensingHandler(void);
__attribute__((weak)) void midiResetHandler(void);
__attribute__((weak)) void midiSysexStartHandler(void);
__attribute__((weak)) void midiSysexDataHandler(unsigned char data);
__attribute__((weak)) void midiSysexEndHandler(void);

#endif /* MIDIHANDLER_HPP_ */
