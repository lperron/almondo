/*
 * GPIOPin.cpp
 *
 *  Created on: Mar 24, 2020
 *      Author: lperron
 */

#include <GPIOPin.h>

GPIOPin::GPIOPin() {
	// TODO Auto-generated constructor stub
	;
}

GPIOPin::~GPIOPin() {
	// TODO Auto-generated destructor stub
	;
}


void GPIOPin:: init(GPIO_TypeDef *GPIOx, uint32_t pin, GPIOMode_TypeDef mode, GPIOOType_TypeDef type, GPIOPuPd_TypeDef pupd, GPIOSpeed_TypeDef speed)
{
	GPIO_InitTypeDef IPIN;

	_GPIOx = GPIOx;
	_pin = pin;
	_mode = mode;
	_type = type;
	_speed = speed;
	_pupd = pupd;

	IPIN.GPIO_Pin = _pin;
	IPIN.GPIO_Mode = _mode;
	IPIN.GPIO_OType = _type;
	IPIN.GPIO_Speed = _speed;
	IPIN.GPIO_PuPd =_pupd;
	GPIO_Init(_GPIOx, &IPIN);
}

void GPIOPin::digitalWrite(BitAction act)
{
	if (_mode != GPIO_Mode_OUT)
		return ;
	GPIO_WriteBit(_GPIOx, _pin, act);
}

char GPIOPin::digitalRead()
{
	if (_mode != GPIO_Mode_IN)
		return (Bit_RESET);
	return(GPIO_ReadInputDataBit(_GPIOx, _pin));
}
