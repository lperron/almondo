/*
 * cs43l22.cpp
 *
 *  Created on: Mar 30, 2020
 *      Author: lperron
 */

#include "cs43l22.hpp"
#include "stmtime.h"

volatile int8_t DMA_StatusFlag = 0;

int16_t _DMA_Buffer[DMA_BUFFER_SIZE];

extern "C" void DMA1_Stream7_IRQHandler()
{
	uint32_t reg;

	reg = DMA1->HISR; //(DMA_GetFlagStatus(DMA1_Stream7, DMA_FLAG_TCIF7) != RESET)
	reg &= (uint32_t)0x0F7D0F7D; //reserved mask
	if ((reg & DMA_FLAG_TCIF7) != RESET)
	{
		DMA_StatusFlag = DMA_TC;
		DMA1->HIFCR = (uint32_t)(DMA_FLAG_TCIF7 & 0x0F7D0F7D); //DMA_ClearFlag
	}
	else 	if ((reg & DMA_FLAG_HTIF7) != RESET)
	{
		DMA_StatusFlag = DMA_HT;
		DMA1->HIFCR = (uint32_t)(DMA_FLAG_HTIF7 & 0x0F7D0F7D);
	}
}

cs43l22::cs43l22()
{
	_addr = 0x94;
	_I2Cx = I2C1;
}

#if 0
//old function, see eval_audio
uint32_t cs43l22::read_register(uint8_t RegisterAddr)
{
  uint32_t result = 0;

  /*!< While the bus is busy */
  while(I2C_GetFlagStatus(_I2Cx, I2C_FLAG_BUSY))
	  ;
  /* Start the config sequence */
  I2C_GenerateSTART(_I2Cx, ENABLE);
  /* Test on EV5 and clear it */
  while (!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_MODE_SELECT))
	  ;
  /* Transmit the slave address and enable writing operation */
  I2C_Send7bitAddress(_I2Cx, _addr, I2C_Direction_Transmitter);
  /* Test on EV6 and clear it */
  while (!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
	  ;
  /* Transmit the register address to be read */
  I2C_SendData(_I2Cx, RegisterAddr);
  /* Test on EV8 and clear it */
  while (I2C_GetFlagStatus(_I2Cx, I2C_FLAG_BTF) == RESET)
	  ;
  /*!< Send START condition a second time */
  I2C_GenerateSTART(_I2Cx, ENABLE);
  /*!< Test on EV5 and clear it (cleared by reading SR1 then writing to DR) */
  while(!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_MODE_SELECT))
	  ;
  /*!< Send Codec address for read */
  I2C_Send7bitAddress(_I2Cx, _addr, I2C_Direction_Receiver);
  /* Wait on ADDR flag to be set (ADDR is still not cleared at this level */
  while(I2C_GetFlagStatus(_I2Cx, I2C_FLAG_ADDR) == RESET)
	  ;
  /*!< Disable Acknowledgment */
  I2C_AcknowledgeConfig(_I2Cx, DISABLE);
  /* Clear ADDR register by reading SR1 then SR2 register (SR1 has already been read) */
  (void)_I2Cx->SR2;
  /*!< Send STOP Condition */
  I2C_GenerateSTOP(_I2Cx, ENABLE);
  /* Wait for the byte to be received */
  while(I2C_GetFlagStatus(_I2Cx, I2C_FLAG_RXNE) == RESET)
	  ;
  /*!< Read the byte received from the Codec */
  result = I2C_ReceiveData(_I2Cx);
  /* Wait to make sure that STOP flag has been cleared */
  while(_I2Cx->CR1 & I2C_CR1_STOP)
	  ;
  /*!< Re-Enable Acknowledgment to be ready for another reception */
  I2C_AcknowledgeConfig(_I2Cx, ENABLE);
  /* Clear AF flag for next communication */
  I2C_ClearFlag(_I2Cx, I2C_FLAG_AF);
  /* Return the byte read from Codec */
  return result;
}
#endif


uint32_t cs43l22::read_register(uint8_t RegisterAddr)
{
	uint32_t result = 0;

	while (((uint32_t)_I2Cx->SR2 & ((I2C_FLAG_BUSY & 0x00FFFFFF) >> 16)) != RESET)//0x00FFFFFF = flag mask
		;
	_I2Cx->CR1 |= I2C_CR1_START;
	while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_MODE_SELECT) != I2C_EVENT_MASTER_MODE_SELECT)
		;
	_I2Cx->DR = _addr & (uint8_t)~((uint8_t)I2C_OAR1_ADD0);
	while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)
		;
	_I2Cx->DR = RegisterAddr;
	while (((uint32_t)_I2Cx->SR1 & ((I2C_FLAG_BTF & 0x00FFFFFF))) == RESET)
		;
	_I2Cx->CR1 |= I2C_CR1_START;
	while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_MODE_SELECT) != I2C_EVENT_MASTER_MODE_SELECT)
		;
	_I2Cx->DR = _addr | I2C_OAR1_ADD0;
	while (((uint32_t)_I2Cx->SR1 & ((I2C_FLAG_ADDR & 0x00FFFFFF))) == RESET)
		;
	_I2Cx->CR1 &= (uint16_t)~((uint16_t)I2C_CR1_ACK);
	(void)_I2Cx->SR2;
	_I2Cx->CR1 |= I2C_CR1_STOP;
	while (((uint32_t)_I2Cx->SR1 & ((I2C_FLAG_RXNE & 0x00FFFFFF))) == RESET)
		;
	result = _I2Cx->DR;
	while(_I2Cx->CR1 & I2C_CR1_STOP)
		;
	_I2Cx->CR1 |= I2C_CR1_ACK;
	_I2Cx->SR1 = (uint16_t)~(I2C_FLAG_AF & 0x00FFFFFF);
	return result;
}

#if 0
//see eval audio
uint32_t cs43l22::write_register(uint8_t RegisterAddr, uint8_t RegisterValue)
{
	 uint32_t result = 0;

	  /*!< While the bus is busy */
	  while(I2C_GetFlagStatus(_I2Cx, I2C_FLAG_BUSY))
		  	 ;
	  /* Start the config sequence */
	  I2C_GenerateSTART(_I2Cx, ENABLE);
	  /* Test on EV5 and clear it */
	  while (!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_MODE_SELECT))
		  ;
	  /* Transmit the slave address and enable writing operation */
	  I2C_Send7bitAddress(_I2Cx, _addr, I2C_Direction_Transmitter);
	  /* Test on EV6 and clear it */
	  while (!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED))
		  ;
	  /* Transmit the first address for write operation */
	  I2C_SendData(_I2Cx, RegisterAddr);
	  /* Test on EV8 and clear it */
	  while (!I2C_CheckEvent(_I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTING))
		  ;
	  /* Prepare the register value to be sent */
	  I2C_SendData(_I2Cx, RegisterValue);
	  /*!< Wait till all data have been physically transferred on the bus */
	  while(!I2C_GetFlagStatus(_I2Cx, I2C_FLAG_BTF))
		  ;
	  /* End the configuration sequence */
	  I2C_GenerateSTOP(_I2Cx, ENABLE);
	#ifdef VERIFY_WRITTENDATA
	  /* Verify that the data has been correctly written */
	  result = (read_register(RegisterAddr) == RegisterValue)? 0:1;
	#endif /* VERIFY_WRITTENDATA */

	  /* Return the verifying value: 0 (Passed) or 1 (Failed) */
	  return result;
}
#endif

uint32_t cs43l22::write_register(uint8_t RegisterAddr, uint8_t RegisterValue)
{
	uint32_t result = 0;

	 while (((uint32_t)_I2Cx->SR2 & ((I2C_FLAG_BUSY & 0x00FFFFFF) >> 16)) != RESET)
	  	 ;
	 _I2Cx->CR1 |= I2C_CR1_START;
	 while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_MODE_SELECT) != I2C_EVENT_MASTER_MODE_SELECT)
		 ;
	 _I2Cx->DR = _addr & (uint8_t)~((uint8_t)I2C_OAR1_ADD0);
	 while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) != I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)
	  ;
	  _I2Cx->DR = RegisterAddr;
	  while ((((((uint32_t)_I2Cx->SR1) | (((uint32_t)_I2Cx->SR2) << 16)) & 0x00FFFFFF) & I2C_EVENT_MASTER_BYTE_TRANSMITTING) != I2C_EVENT_MASTER_BYTE_TRANSMITTING)
		  ;
	  _I2Cx->DR = RegisterValue;
	  while (((uint32_t)_I2Cx->SR1 & ((I2C_FLAG_BTF & 0x00FFFFFF))) == RESET)
		  ;
	  _I2Cx->CR1 |= I2C_CR1_STOP;
	#ifdef VERIFY_WRITTENDATA
	  result = (read_register(RegisterAddr) == RegisterValue)? 0:1;
	#endif
	  return result;
}

uint32_t cs43l22::set_volume(uint8_t Volume)
{
	  uint32_t counter = 0;

	  if (Volume > 0xE6)
	  {
	    /* Set the Master volume */
	    counter += write_register(0x20, Volume - 0xE7);
	    counter += write_register(0x21, Volume - 0xE7);
	  }
	  else
	  {
	    /* Set the Master volume */
	    counter += write_register(0x20, Volume + 0x19);
	    counter += write_register(0x21, Volume + 0x19);
	  }

	  return counter;
}

uint32_t cs43l22::init_registers()
{
	uint32_t counter;
	uint8_t tmp;
	counter = 0;
//	GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_SET);
	counter += write_register(0x02, 0x01);  //power down

	counter += write_register(0x04, 0xAF); //speqkers
//	GPIO_WriteBit(GPIOD, GPIO_Pin_15, Bit_SET);
	counter += write_register(0x05, 0x81); //clock auto detect
	counter += write_register(0x06, 0x07); //audio std
	counter += set_volume(210);
	counter += write_register(0x00, 0x99);
	counter += write_register(0x47, 0x80);
	tmp = (uint8_t)read_register(0x32);
	tmp |= 1 << 7;
	counter += write_register(0x32, tmp);
	tmp &= 0x7F;
	counter += write_register(0x32, tmp);
	counter += write_register(0x00, 0x00);
	counter += write_register(0x02, 0x9E);  //power on
	//nice power off config
	counter += write_register(0x0A, 0x00);  //disable analog soft ramp
	counter += write_register(0x0E, 0x04);
	counter += write_register(0x27, 0x00); //disable limiter attack lvl
	counter += write_register(0x1F, 0x0F); //adj bass n treble
	counter += write_register(0x1A, 0x0A); //adjust pcm lvl
	counter += write_register(0x1B, 0x0A);//same other channel

	return (counter);
}

void cs43l22::init()
{
	GPIO_InitTypeDef PinInitStruct;
	I2C_InitTypeDef I2C_InitType;
	I2S_InitTypeDef I2S_InitType;
	//clocks and pll things
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1 | RCC_APB1Periph_SPI3, ENABLE);
	RCC_PLLI2SCmd(ENABLE);
	//pins

	//PD4 RESET AUDIO_RST /////
	PinInitStruct.GPIO_Pin = GPIO_Pin_4;
	PinInitStruct.GPIO_Mode = GPIO_Mode_OUT;
	PinInitStruct.GPIO_OType = GPIO_OType_PP;
	PinInitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	PinInitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &PinInitStruct);

	//I2C PIN
	//PB9 SDA I2C /////
	//PB6 SCL I2C//////
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);
    GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1);
	PinInitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_9;
	PinInitStruct.GPIO_Mode = GPIO_Mode_AF;
	PinInitStruct.GPIO_OType = GPIO_OType_OD;
	PinInitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	PinInitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &PinInitStruct);
	//I2C INIT
	I2C_InitType.I2C_ClockSpeed = 100000;
	I2C_InitType.I2C_Mode = I2C_Mode_I2C;
	I2C_InitType.I2C_OwnAddress1 = 0x33;
	I2C_InitType.I2C_Ack = I2C_Ack_Enable;
	I2C_InitType.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_InitType.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_Cmd(I2C1, ENABLE);
	I2C_Init(I2C1, &I2C_InitType);

	//I2S pins
	//PC7 MCLK I2S3MCK
	//PC10 SCLK I2S3SCK
	//PC12 SDIN I2S3SD
	//PA4 LRCK I2S3WS
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource4, GPIO_AF_SPI3); // I2S3_WS
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_SPI3); // I2S3_MCK
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_SPI3); // I2S3_sCK
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_SPI3); // I2S3_SD
	PinInitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	PinInitStruct.GPIO_Mode = GPIO_Mode_AF;
	PinInitStruct.GPIO_OType = GPIO_OType_PP;
	PinInitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	PinInitStruct.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOA, &PinInitStruct);
	PinInitStruct.GPIO_Pin = GPIO_Pin_7 | GPIO_Pin_10 | GPIO_Pin_12;
	GPIO_Init(GPIOC, &PinInitStruct);

	//I2S INIT
	I2S_InitType.I2S_AudioFreq = I2S_AudioFreq_48k;
	I2S_InitType.I2S_MCLKOutput = I2S_MCLKOutput_Enable;
	I2S_InitType.I2S_Mode = I2S_Mode_MasterTx;
	I2S_InitType.I2S_DataFormat = I2S_DataFormat_16b;
	I2S_InitType.I2S_Standard = I2S_Standard_Phillips;
	I2S_InitType.I2S_CPOL = I2S_CPOL_Low;
	I2S_Init(SPI3, &I2S_InitType);
	I2S_Cmd(SPI3, ENABLE);

	GPIO_SetBits(GPIOD, GPIO_Pin_4); //turn on dac

	delay_ms(1);//DELAY

	init_registers();

	//some DMA stuff
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	int i = -1;
	while (++i < DMA_BUFFER_SIZE)
		_DMA_Buffer[i] = 0;
	DMA_InitTypeDef DMA_InitType;
	DMA_Cmd(DMA1_Stream7, DISABLE);
	DMA_DeInit(DMA1_Stream7);

	DMA_InitType.DMA_Channel = DMA_Channel_0;
	DMA_InitType.DMA_PeripheralBaseAddr = (uint32_t)0x40003C0C;
	DMA_InitType.DMA_Memory0BaseAddr = (uint32_t)_DMA_Buffer;
	DMA_InitType.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitType.DMA_BufferSize = (uint32_t)DMA_BUFFER_SIZE;
	DMA_InitType.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitType.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitType.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitType.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitType.DMA_Mode = DMA_Mode_Circular;
	DMA_InitType.DMA_Priority = DMA_Priority_High;
	DMA_InitType.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitType.DMA_FIFOThreshold = DMA_FIFOThreshold_1QuarterFull;
	DMA_InitType.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitType.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(DMA1_Stream7, &DMA_InitType);

	DMA_ITConfig(DMA1_Stream7, DMA_IT_TC | DMA_IT_HT, ENABLE);

	// Enable the IRQ.
    NVIC_InitTypeDef nvicStructure;
    nvicStructure.NVIC_IRQChannel = DMA1_Stream7_IRQn;
    nvicStructure.NVIC_IRQChannelPreemptionPriority = 0;
    nvicStructure.NVIC_IRQChannelSubPriority = 0;
    nvicStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&nvicStructure);
  // Start DMA to codec.
    SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Tx, ENABLE);
    DMA_Cmd(DMA1_Stream7, ENABLE);
}
