/*
 * addit.cpp
 *
 *  Created on: Apr 3, 2020
 *      Author: lperron
 */

#include "addit.hpp"
#include "wheels.h"

extern volatile int8_t DMA_StatusFlag;
#ifdef HAMMOND_TEMP

float notes_from_zero[12] = {32.703f, 34.6454737762054f, 36.7243358648789f,38.9016078488908f, 41.2134748390451f, 43.6506353141884f, 46.2651264880057f, 49.016025900806f, 51.9088636006511f, 55.0179882560068f, 58.2799243133968f, 61.7344699449238f};

#else

float notes_from_zero[12] = {32.703f, 34.648f, 36.708f, 38.891f, 41.203f ,43.654f, 46.249f, 48.999f, 51.913f, 55.0f, 58.270f, 61.735f};

#endif
int8_t offset_harm[NB_DRAWBARS] = {-12, 7, 0, 12, 19, 24, 28, 31, 36};


float get_freq(char note)
{
	if (note <= 0)
		return (32.703f);
	if (note >= 12)
		return (61.735f);
	return (notes_from_zero[(int)note]);
}

uint16_t get_buff_size(float freq)
{
	return (SAMPLE_RATE / freq);
}

t_zero_sine_buffer	get_buffer(char note)
{
	t_zero_sine_buffer ret;
	ret.size = wheel_size[(int)note];
	ret.buffer = wheels_lut[(int)note];
	return (ret);
}

t_zero_sine_buffer	*create_all_sines()
{
	t_zero_sine_buffer *ret;
	int	i;

	if (!(ret = (t_zero_sine_buffer *)malloc(sizeof(t_zero_sine_buffer) * 12)))
		return (NULL);
	i = -1;
	while (++i < 12)
		ret[i] = get_buffer(i); //start from C1
	return (ret);
}

void	free_all_sines(t_zero_sine_buffer *tofree)
{
	free(tofree);
}

__attribute__ ((hot))  inline float hardclip(float value)
{
	const float t = value < -32768.f ? -32768.f : value;
	return t > 32767.f ? 32767.f : t;
}
/*
__attribute__ ((hot)) inline float softclip(float value)
{
	if (value < 0)
		return ((-1 * (1.f - expf(value))));
		else
			return (((1.f - expf(-1.f * value))));
}
*/
void addit::init_wheels()
{
	int i;
	uint8_t note;
	uint8_t oct;
#ifdef RND_PHASE
	uint32_t rnd_phase;
	float start;
#endif

	i = -1;
	while (++i < NB_TONE_WHEELS)
	{

		note = i % 12;
		oct = i / 12;
		_tone_wheels[i].buffer_ref = _sines[note].buffer;
		_tone_wheels[i].size = _sines[note].size;
		_tone_wheels[i].end = _tone_wheels[i].buffer_ref + _tone_wheels[i].size - 1;
		_tone_wheels[i].speed = 1 << oct;

#ifdef RND_PHASE
		while(RNG_GetFlagStatus(RNG_FLAG_DRDY)== RESET)
			;
		rnd_phase = RNG_GetRandomNumber();
		start = rnd_phase / (float)UINT_LEAST32_MAX;
		_tone_wheels[i].buffer = &_sines[note].buffer[(uint32_t)(start *(_tone_wheels[i].size - 1))];
		if (_tone_wheels[i].buffer - _tone_wheels[i].buffer_ref >= _tone_wheels[i].size)
			_tone_wheels[i].buffer -= _tone_wheels[i].size;
#else
		_tone_wheels[i].buffer = _sines[note].buffer;
#endif
	}
	i = -1;
	while (++i < NB_TONE_WHEELS)
	{
		if (i < 60)
			_tone_wheels[i].leaking = &_tone_wheels[i + 48];
		else
			_tone_wheels[i].leaking = &_tone_wheels[i - 48];
	}
}

void addit::set_voice_tone(int note, t_tone_wheels **harmonics) //midi note, from C2
{
	int i;

	if (note < MIN_NOTE)
		note = MIN_NOTE;
	else if (note > MAX_NOTE)
		note = MAX_NOTE;
	note -= MIN_NOTE;
	note += 12; //0 is for the sub @ minus one oct
	i = -1;
	while (++i < NB_DRAWBARS)
		harmonics[i] = &_tone_wheels[note + offset_harm[i]];
}

addit::addit()
{
	int i;

#ifdef RND_PHASE
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE); //TRNG
	RNG_Cmd(ENABLE);
#endif
	value = 0;
	_sines = create_all_sines();
	init_wheels();
	_sustained = false;
	_sostenuto = false;
	i = -1;
	while (++i < NB_VOICES)
	{
		_voices[i].key = 0;
		_voices[i].gate = STATE_OFF;
		_voices[i].env = 0.f;
		set_voice_tone(66, _voices[i].harmonics);
	}
	_leaks_on = false;
	_leaks = 0.01f;
	_nb_voices = NB_VOICES;
	_destroy_factor = 0.f;
	_lvl = 1.f / 16.f;
	_attack = 0.0080645f; //attack decay and release are value for a midi CC 2 (or 1, I can't remember, in any case a small one)
	_decay = 0.0000403f;
	_decayMidiValue = 2;
	_sustain = 0.95f;
	_release = 0.0080645f;
	_HARlvl[0] = 1.f; //Jimmy Smith Style
	_HARlvl[1] = 1.f;
	_HARlvl[2] = 1.f;
	i = 2;
	while (++i < NB_DRAWBARS)
		_HARlvl[i] = 0.f;
	i = -1;
	while (++i < NB_DRAWBARS)
		_HARlvlCorrected[i] = _HARlvl[i] * _lvl;
	_vbr->set_width(0.4f); //well using only 40 percent of the vbr buff we could downsize it!
	_vbr->set_freq(6.f);
	_vibrato_factor = 0.f;

#ifdef RND_PHASE //we won't use TRNG anymore
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, DISABLE);
	RNG_Cmd(DISABLE);
#endif
}

addit::~addit()
{
	free_all_sines(_sines);
	//other stuff to put here but I do not care
}


void addit::setHARlvl(unsigned char val, unsigned char id)
{
	_HARlvl[id] = val / 127.f;
	_HARlvlCorrected[id] = _HARlvl[id] * _lvl;
}
void addit::setAttack(unsigned char val)
{
	float tmp = val / 127.f;
	tmp = tmp * tmp;
	tmp *= 500000.f ;
	tmp = tmp == 0.f ? 1.f : tmp;
	const float oldValue = _attack;
	_attack = FSAMPLES_COUNT_BEFORE_ENV_UPDATE /tmp;
	int i = -1; //won't be problematic until a long time
	while (++i < _nb_voices)
	{
		if (_voices[i].envIncr == oldValue)
			_voices[i].envIncr = _attack;
	}
}
void addit::setDecay(unsigned char val)
{
	_decayMidiValue = val;
	float tmp = val / 127.f;
	tmp = tmp * tmp;
	tmp *= 500000.f;
	tmp = tmp == 0.f ? 1.f : tmp;
	const float oldValue = _decay;
	_decay = FSAMPLES_COUNT_BEFORE_ENV_UPDATE * (1.f - _sustain)/tmp;
	int i = -1;
	while (++i < _nb_voices)
	{
		if (_voices[i].gate && _voices[i].envIncr == -oldValue) //sustained or decaying
			_voices[i].envIncr = -_decay;
	}
}
void addit::setSustain(unsigned char val)
{
	_sustain = val / 127.f;
	float tmp = _decayMidiValue / 127.f;
	tmp = tmp * tmp;
	tmp *= 500000.f;
	tmp = tmp == 0.f ? 1.f : tmp;
	const float oldValue = _decay;
	_decay = FSAMPLES_COUNT_BEFORE_ENV_UPDATE * (1.f - _sustain)/tmp;
	int i = -1;
	while (++i < _nb_voices)
	{
		if (_voices[i].gate && _voices[i].envIncr == -oldValue) //sustained or decaying
		{
			_voices[i].envIncr = -_decay;
			_voices[i].envMin = _sustain;
		}
	}
}
void addit::setLvl(unsigned char val)
{
	_lvl = (val / 254.f);
	int i = -1;
	while (++i < NB_DRAWBARS)
		_HARlvlCorrected[i] = _HARlvl[i] * _lvl;
}
void addit::setDestr(unsigned char val)
{
	_destroy_factor = val / 127.f;
}
void addit::setVibrato(unsigned char val)
{
	_vibrato_factor = val / 254.f;
}
void addit::setRelease(unsigned char val)
{
	float tmp = val / 127.f;
	tmp = tmp * tmp;
	tmp *= 500000.f;
	tmp = tmp == 0.f ? 1.f : tmp;
	_release = FSAMPLES_COUNT_BEFORE_ENV_UPDATE /tmp;
	int i = -1;
	while (++i < _nb_voices)
	{
		if (!_voices[i].gate && _voices[i].env > 0) //releasing
		{
			_voices[i].envIncr = -_release;
		}
	}
}

void addit::setSustainOn(unsigned char val)
{
	if (val >= 64)
	{
		_sustained = true;
	}
	else
	{
		_sustained = false;
		int i = -1;
		while (++i < _nb_voices)
		{
			if (_voices[i].gate == STATE_SUS)
			{
				_voices[i].gate = STATE_OFF;
				_voices[i].timeoff = millis();
				_voices[i].key = 0;
				_voices[i].envIncr = - _release;
				_voices[i].envMin = 0.f;
			}
		}
	}
}

void addit::setSostenutoOn(unsigned char val)
{
	int i;
	if (val >= 64)
	{
		_sostenuto = true;
		i = -1;
		while (++i < _nb_voices)
		{
			if (_voices[i].gate == STATE_ON)
			{
				_voices[i].gate = STATE_SOSP;
			}
		}
	}
	else
	{
		_sostenuto = false;
		i = -1;
		while (++i < _nb_voices)
		{
			if (_voices[i].gate == STATE_SOSR)
			{
				_voices[i].gate = STATE_OFF;
				_voices[i].timeoff = millis();
				_voices[i].key = 0;
				_voices[i].envIncr = - _release;
				_voices[i].envMin = 0.f;
			}
		}
	}
}

void addit::setLeaksLvl(unsigned char val)
{
	float tmp = val / 127.f;
	tmp *= tmp * tmp;
	_leaks = tmp * 0.2f;
}

void addit::setLeaksOn(unsigned char val)
{
	if (val >= 64 && !_leaks_on)
			switch_leaks();
	else if (val < 64 && _leaks_on)
			switch_leaks();
}

__attribute__ ((hot)) void addit::noteon(char note, char vel)
{
	int i = -1;
	int best;
	uint32_t btime;

	(void)vel;
	if (note < MIN_NOTE || note >= MAX_NOTE)
		return ;
	btime = UINT_LEAST32_MAX;
	while (++i < _nb_voices)
	{
		if (!_voices[i].gate && _voices[i].timeoff < btime)
		{
			btime = _voices[i].timeoff;
			best = i;
		}
	}
	if (btime != UINT_LEAST32_MAX) //this can cause a bug, but it will really be bad luck :D
	{
		_voices[best].key = note;
		set_voice_tone(note, _voices[best].harmonics);
		_voices[best].gate = STATE_ON;
		_voices[best].env = 0.f;
		_voices[best].envIncr = _attack;
		_voices[best].envMin = _sustain;
		return ;
	}
	//the best fix is to not release a key after exactly 49.71026962962962962962962962963 days of use...
}

__attribute__ ((hot)) void addit::noteoff(char note)
{
	int i = -1;

	if (note < 21)
		return ;
	while (++i < _nb_voices)
	{
		if (_voices[i].gate != STATE_OFF && _voices[i].gate != STATE_SUS && note == _voices[i].key)
		{
			if (!_sustained)
			{
				if ((_voices[i].gate == STATE_ON) || (!_sostenuto))
				{
					_voices[i].gate = STATE_OFF;
					_voices[i].timeoff = millis();
					_voices[i].key = 0;
					_voices[i].envIncr = -_release;
					_voices[i].envMin = 0.f;
				}
				else //sostenuto
					_voices[i].gate = STATE_SOSR;

			}
			else //sustain
				_voices[i].gate = STATE_SUS;
		}
	}
}

__attribute__ ((hot)) inline void addit::getEnvs() //need a better comparison between getEnv(i) n getEnvs()
{
	int i = -1;

	//this should be more efficient
	//more vector friendly
	//and more switch unfriendly
	//stateless
	//plus always worst case, And I Like That

	//for each voice we can use an incr and a low
	//low = sustain
	//then just add the incr; //keep in mind that incr must be signed
	//when note triggered incr = att;
	//then if (env > 1f)
	//incr = decay
	//then do nothing if lvl < sustain
	//clamp (low, 1f)
	//when note released
	//low = 0f;
	//incr = release
	while (++i < _nb_voices)
	{
		_voices[i].env += _voices[i].envIncr;
		if (_voices[i].env > 1.f)
		{
			_voices[i].envIncr = -_decay;
			_voices[i].env = 1.f;
		}
		else if (_voices[i].env < _voices[i].envMin && _voices[i].envIncr < 0.f) //sustain or release
		{
			_voices[i].env = _voices[i].envMin;
		}
	}
}

__attribute__ ((hot))  inline void addit::turn_wheels()
{
	char i;
	t_tone_wheels *curr;

	i = NB_TONE_WHEELS;
	curr = _tone_wheels;
	while (i--)
	{
		curr->buffer += curr->speed;
		if (curr->buffer  > curr->end)
			curr->buffer -= curr->size;
		curr++;
	}
}

void addit::allNoteOff() //soft all notes off
{
	for (int i = 0; i < NB_VOICES; i++)
	{
	if ((_voices[i].gate != STATE_OFF))
	{
			_voices[i].gate = STATE_OFF;
			_voices[i].timeoff = millis();
			_voices[i].key = 0;
			_voices[i].envIncr = -_release;
			_voices[i].envMin = 0.f;
		}
	}
}

void addit::panic() //hard all notes off
{
	int i;

	i = -1;
	while (++i < _nb_voices)
	{
		_voices[i].gate = STATE_OFF;
		_voices[i].timeoff = millis();
		_voices[i].env = 0.f;
		_voices[i].envIncr = 0.f;
	}
}

void addit::switch_leaks()
{
	panic();
	if (_leaks_on)
	{
		_nb_voices = NB_VOICES;
		GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_RESET);
		_leaks_on = false;
	}
	else
	{
		_nb_voices = NB_VOICES_LEAKS;
		GPIO_WriteBit(GPIOD, GPIO_Pin_13, Bit_SET);
		_leaks_on = true;
	}
}

__attribute__ ((hot)) void addit::fillBuff() //I do not like the "if (leaks on) else" design
{

	float tmpval;
	uint8_t i;
	uint8_t j;
	int16_t sample;
	int16_t tobuff;
	int16_t offset;

	if (DMA_StatusFlag == DMA_NOP)
		return ;
	offset = DMA_StatusFlag == DMA_HT ? 0 : DMA_BUFFER_HALF_SIZE;
	sample = -1;
	i = -1;
	if (_leaks_on)
	{
		while (++sample < DMA_BUFFER_HALF_SIZE)
		{
			value = 0.f;
			i = -1;
			turn_wheels();
			if ((sample % (SAMPLES_COUNT_BEFORE_ENV_UPDATE << 1)) == 0)
				getEnvs();
			while (++i < _nb_voices)
			{
				j = -1;
				tmpval = 0.f;
				while (++j < NB_DRAWBARS)
				{
					tmpval += (*(_voices[i].harmonics[j]->buffer) + _leaks * *(_voices[i].harmonics[j]->leaking->buffer))* _HARlvlCorrected[j];
				}
				value += tmpval * _voices[i].env;
			}
			value = hardclip(value);
			tobuff = (int16_t)value + _destroy_factor * ((uint16_t) value - (int16_t) value); //this and the next line are just lerping variations
			tobuff += _vibrato_factor * (_vbr->vibrato_effect(tobuff) - tobuff);
			_DMA_Buffer[sample + offset] = (int16_t)tobuff;
			_DMA_Buffer[++sample + offset] = (int16_t)tobuff;
		}
	}
	else
	{
		while (++sample < DMA_BUFFER_HALF_SIZE)
		{
			value = 0.f;
			i = -1;
			turn_wheels();
			if ((sample % (SAMPLES_COUNT_BEFORE_ENV_UPDATE << 1)) == 0)
				getEnvs();
			while (++i < _nb_voices)
			{
				j = -1;
				tmpval = 0.f;
				while (++j < NB_DRAWBARS)
				{
					tmpval += (*(_voices[i].harmonics[j]->buffer) * _HARlvlCorrected[j]);
				}
				value += tmpval * _voices[i].env;
			}
			value = hardclip(value);
			tobuff = (int16_t)value + _destroy_factor * ((uint16_t) value - (int16_t) value);
			tobuff += _vibrato_factor * (_vbr->vibrato_effect(tobuff) - tobuff);
			_DMA_Buffer[sample + offset] = (int16_t)tobuff;
			_DMA_Buffer[++sample + offset] = (int16_t)tobuff;
		}
	}
	DMA_StatusFlag = DMA_NOP;
}
