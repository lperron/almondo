/*
 * midiControls.h
 *
 *  Created on: May 3, 2020
 *      Author: lperron
 */

#ifndef MIDICONTROLS_H_
#define MIDICONTROLS_H_


//CC arre mapped on a mopho keyboard
#define USER_MIDI_IN_CHANNEL 1

#define MIDISUST 64
#define MIDISOST 66

#define CCDESTR 28

#define CCENVLVL 86
#define CCENVA 89
#define CCENVD 90
#define CCENVS 77
#define CCENVR 78

#define CCHAR1 106
#define CCHAR2 109
#define CCHAR3 110
#define CCHAR4 111
#define CCHAR5 112
#define CCHAR6 115
#define CCHAR7 118
#define CCHAR8 119
#define CCHAR9 75

#define CCSWITCH_LEAKS 31
#define CCLEAKS 29

#define CCMOD 1

//channel messages mode

#define ALLSOUNDSOFF 120
#define RESETCONTROLLER 121
#define LOCALCONTROL 122 //no use coz' no local control
#define ALLNOTESOFF 123

#endif /* MIDICONTROLS_H_ */
