/*
 * midi.cpp
 *
 *  Created on: Mar 27, 2020
 *      Author: lperron
 *
 *
 *     notes :
 *     rx = a10
 *     tx = a9
 *     tx will need a level shifter, Logic =~ 3V
 *     rx is fine, 5V tolerance on stm32f4
 */

#include "stm32f4xx.h"
#include "midi.hpp"
#include "string.h"

Midi MIDI;


static inline unsigned char is_realtime(unsigned char message)
{
	return ((message & 0xF8) == 0xF8);
}

static inline unsigned char is_status(unsigned char message)//ugly
{
	return (message >> 7);
}

//USB MIDI callback
extern "C" uint16_t USB_MIDI_DataRx(uint8_t *msg)
{
	//the lazy way
	//also some messages won't be handled (ex sysex and tune request, which are handled @ reception and not @ completion for non USB midi)
	//coulb be handled like realtime, but no need atm
	if (is_realtime(msg[0]))
	{
		MIDI.handle_realtime(msg[0]);
		return 0;
	}
	MIDI.handle_message(msg);
	return 0;
}

extern "C" void USART2_IRQHandler(void)
{
	if (USART2->SR & USART_FLAG_RXNE)
		MIDI.putDataInMidiInBuffer(USART2->DR  & (uint16_t)0x01FF);
}

/*********************/
/*                   */
/*    Midi Class     */
/*                   */
/*********************/


void	Midi::putDataInMidiInBuffer(unsigned char data)
{
	_midiInBuff.write(data);
}

void	Midi::sysex_parser(unsigned char data) //boarf, not my job here, three handler, start data end
{
	if (is_realtime(data))
	{
		handle_realtime(data);
		return ;
	}
	else if (data == SYSEX_END)
	{
		midiSysexEndHandler();
		_running = 0;
	}
	else
		midiSysexDataHandler(data);
}

void Midi::status_parser(unsigned char data)
{
	if (is_realtime(data))
	{
		handle_realtime(data);
		return ;
	}
	_running = data;
	_third = 0;
	if (data == TUNE_REQUEST)
		midiTuneRequestHandler();
	if (data == SYSEX)
		midiSysexStartHandler();
}

void Midi::two_bytes_mes(unsigned char data)
{
	_message[0] = _running;
	_message[1] = data;
	handle_message(_message);
}

void Midi::three_bytes_mes(unsigned char data)
{
	_third = 1;
	_message[0] = _running;
	_message[1] = data;
}

/* PLEASE REFER TO MIDI SPECIFICATIONS*/

void Midi::data_parser(unsigned char data)
{
	if (_third) //last byte of size three message
	{
		_third = 0;
		_message[2] = data;
		handle_message(_message);
		return ;
	}
	else if (!_running) //no valid status, ignore
	{
		return ;
	}
	else if (_running < 0xC0) //three bytes channel voices message other than pitchbend
	{
		three_bytes_mes(data);
	}
	else if (_running < 0xE0) //two bytes channel voice message
	{
		two_bytes_mes(data);
	}
	else if (_running < 0xF0)//pitchbend
	{
		three_bytes_mes(data);
	}
	else if (_running == SONG_POSITION)
	{
		three_bytes_mes(data);
		_running = 0;
	}
	else if (_running == SONG_SELECT || _running == QUARTER_FRAME)
	{
		two_bytes_mes(data);
		_running = 0;
	}
	else //no valid status, ignore
		_running = 0;

}

void	Midi::read()
{
	unsigned char data;

	while (_midiInBuff.readable())
	{
		data =  _midiInBuff.fastRead();
		if (_running == SYSEX)
			sysex_parser(data);
		else if (is_status(data))
			status_parser(data);
		else
			data_parser(data);
	}
}


void	Midi::handle_realtime(unsigned char mes)
{
	if (mes == CLOCK)
		midiClockHandler();
	else if (mes == START)
		midiStartHandler();
	else if (mes == CONTINUE)
		midiContinueHandler();
	else if (mes == STOP)
		midiStopHandler();
	else if (mes == SENSING)
		midiSensingHandler();
	else if (mes == RESET)
		midiResetHandler();

}
void	Midi::handle_message(unsigned char *messa)
{
	unsigned char mes[3];
	unsigned char chan;


	memcpy(mes, messa, sizeof(unsigned char) * 3); //we can't rely on messa since it can be overwritten by the next midi message we receive
	chan = 0;
	if (mes[0] < 0xF0) //not for system common mes
	{
		chan = mes[0] & OMNI;
		if (_channel == OMNI)
			mes[0] |= OMNI; //channel overriding
	}
	if ( mes[0] == (_channel | NOTEON))
		midiNoteOnHandler(chan, mes[1], mes[2]);
	else if (mes[0] == (_channel | NOTEOFF))
		midiNoteOffHandler(chan, mes[1], mes[2]);
	else if (mes[0] == (_channel | CONTROL_CHANGE))
			midiControlChangeHandler(chan, mes[1], mes[2]); //handled realtime
	else if (mes[0] == (_channel | POLYPHONIC_AFTERTOUCH))
		midiPolyphonicAftertouchHandler(chan, mes[1], mes[2]);
	else if (mes[0] == (_channel | PITCHBEND))
		midiPitchbendHandler(chan, ((uint16_t)mes[2] << 7) & mes[1]);
	else if (mes[0] == SONG_POSITION)
		midiSongPositionHandler(((uint16_t)mes[2] << 7) & mes[1]);
	else if (mes[0] == (PROGRAM_CHANGE | _channel))
		midiProgramChangeHandler(chan, mes[1]);
	else if (mes[0] == (CHANNEL_AFTERTOUCH | _channel))
		midiChannelAftertouchHandler(chan, mes[1]);
	else if (mes[0] == QUARTER_FRAME)
		midiQuarterFrameHandler(mes[1]);
	else if (mes[0] == SONG_SELECT)
		midiSongSelectHandler(mes[1]);
}

Midi::	Midi()
{
	_running = false;
	_channel = 0;
	_third = 0;
}

void	Midi::begin(unsigned char channel) //STILL NEED TO MANAGE OMNI MODE ON MIDI IN
{
	GPIO_InitTypeDef GPIO_InitStruct;
	USART_InitTypeDef USART_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	//USART2
	//Init pin A2 A3
	_running = 0;
	if (channel > 15)
		_channel = OMNI;
	else
		_channel = channel;
	memset(_message, 0, sizeof(unsigned char) * 3);
	_third = 0;
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE); //this can be enabled elsewhere but since we only call begin once it's no overkill
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	//init usart2 on A2 A3
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 , ENABLE);
	USART_InitStruct.USART_BaudRate = 31250;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
	USART_InitStruct.USART_Parity = USART_Parity_No;
	USART_InitStruct.USART_StopBits = USART_StopBits_1;
	USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART2, &USART_InitStruct);
	USART_Cmd(USART2, ENABLE);
	//interrupt
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
	//init NVIC
	NVIC_InitStruct.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_Init(&NVIC_InitStruct);
}
