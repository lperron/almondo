/*
 * midiCCRouter.cpp
 *
 *  Created on: Feb 26, 2022
 *      Author: perro
 */

#include "midiCCRouter.hpp"

midiCCRouter::midiCCRouter() {
	;
}


void midiCCRouter::ResetToDefault()
{
	ccBuffer[CCDESTR] = 0;
	ccBuffer[CCENVLVL] = 16;
	ccBuffer[CCENVA] = 2;
	ccBuffer[CCENVD] = 4;
	ccBuffer[CCENVS] = 127;
	ccBuffer[CCENVR] = 4;
	ccBuffer[CCHAR1] = 127;
	ccBuffer[CCHAR2] = 127;
	ccBuffer[CCHAR3] = 127;
	ccBuffer[CCHAR4] = 0;
	ccBuffer[CCHAR5] = 0;
	ccBuffer[CCHAR6] = 0;
	ccBuffer[CCHAR7] = 0;
	ccBuffer[CCHAR8] = 0;
	ccBuffer[CCHAR9] = 0;
	ccBuffer[CCSWITCH_LEAKS] = 0;
	ccBuffer[CCLEAKS] = 0;
	ccBuffer[CCMOD] = 0;
}

void midiCCRouter::Init(addit *almondo)
{
	organ = almondo;
	for (int i = 0; i < 128; i++)
	{
		ccBuffer[i] = 0;
		ccCurrentBuffer[i] = 0;
	}
	ResetToDefault();
}

void midiCCRouter::BufferizeCC(unsigned char cc, unsigned char value)
{
	ccBuffer[cc] = value;
}

void midiCCRouter::ProcessCC()
{
	unsigned char val;
	for (int i = 0; i < 120; i++) //basic CC message
	{
		val = ccBuffer[i];
		if (val != ccCurrentBuffer[i])
		{
			ccCurrentBuffer[i] = val;
			routeCC(i, val);
		}
	}
	for (int i = 120; i < 128; i++) //channel mode
	{
		val = ccBuffer[i];
		if (val != ccCurrentBuffer[i])
		{
			ccCurrentBuffer[i] = val;
			ProcessChannelModeCC(i, val);
		}
	}
}

void midiCCRouter::routeCC(unsigned char cc, unsigned char val)
{
	if (cc == MIDISUST)
		organ->setSustainOn(val);
	else if (cc == MIDISOST)
		organ->setSostenutoOn(val);
	else if (cc == CCENVS)
		organ->setSustain(val);
	else if (cc == CCENVD)
		organ->setDecay(val);
	else if (cc == CCENVA)
		organ->setAttack(val);
	else if (cc == CCENVR)
		organ->setRelease(val);
	else if (cc == CCENVLVL)
		organ->setLvl(val);
	else if (cc == CCHAR1)
		organ->setHARlvl(val, 0);
	else if (cc == CCHAR2)
		organ->setHARlvl(val, 1);
	else if (cc == CCHAR3)
		organ->setHARlvl(val, 2);
	else if (cc == CCHAR4)
		organ->setHARlvl(val, 3);
	else if (cc == CCHAR5)
		organ->setHARlvl(val, 4);
	else if (cc == CCHAR6)
		organ->setHARlvl(val, 5);
	else if (cc == CCHAR7)
		organ->setHARlvl(val, 6);
	else if (cc == CCHAR8)
		organ->setHARlvl(val, 7);
	else if (cc == CCHAR9)
		organ->setHARlvl(val, 8);
	else if (cc == CCDESTR)
		organ->setDestr(val);
	else if (cc == CCMOD)
		organ->setVibrato(val);
	else if (cc == CCSWITCH_LEAKS)
		organ->setLeaksOn(val);
	else if (cc == CCLEAKS)
		organ->setLeaksLvl(val);
}

void midiCCRouter::ProcessChannelModeCC(unsigned char cc, unsigned char value)
{
	if (cc == ALLSOUNDSOFF)
	{
		organ->panic();
	}
	else if (cc == RESETCONTROLLER)
	{
		ResetToDefault();
	}
	else if (cc >= ALLNOTESOFF)
	{
		organ->allNoteOff();
		//currently not handling mono/poly + omni
		//need to rework voice assignment in almondo's code to be able to do it
	}
}

midiCCRouter::~midiCCRouter() {
	;
}

