/*
 * stmtime.h
 *
 *  Created on: Mar 26, 2020
 *      Author: lperron
 */

#ifndef STMTIME_H_
#define STMTIME_H_

void systickInit(uint16_t frequency);
void	systickInit_ms(void);
uint32_t millis();
void delay_ms (uint32_t t);



#endif /* STMTIME_H_ */
