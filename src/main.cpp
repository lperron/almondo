#define  ARM_MATH_CM4
#define  VFP_FP
#include <math.h>
#include "stm32f4xx.h"
#include "usbd_usr.h"
#include "usbd_desc.h"
#include "cs43l22.hpp"
#include "GPIOPin.h"
#include "stmtime.h"
#include "midiControls.h"
#include "midi.hpp"
#include "addit.hpp"
#include "usbd_midi_core.h"
#include "usbd_midi_user.h"
#include "midiCCRouter.hpp"


__ALIGN_BEGIN USB_OTG_CORE_HANDLE  USB_OTG_dev __ALIGN_END;

addit organ;
midiCCRouter midiCCProcessor;
GPIOPin led[4];
long int debounce = 0;


extern "C" void	EXTI0_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line0)!=RESET)
	{

		if (millis() - debounce > 250)
		{
			debounce = millis();
			organ.switch_leaks();
		}
		EXTI_ClearITPendingBit(EXTI_Line0);
	}
}



void init_push_button()
{
	GPIOPin push;
	EXTI_InitTypeDef initPushEXTI;
	NVIC_InitTypeDef initPushNVIC;

	push.init(GPIOA, GPIO_Pin_0, GPIO_Mode_IN, GPIO_OType_PP , GPIO_PuPd_DOWN,  GPIO_Speed_100MHz);
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA , EXTI_PinSource0);
	initPushEXTI.EXTI_Line = EXTI_Line0;
	initPushEXTI.EXTI_Mode = EXTI_Mode_Interrupt;
	initPushEXTI.EXTI_Trigger = EXTI_Trigger_Falling;
	initPushEXTI.EXTI_LineCmd = ENABLE;
	EXTI_Init(&initPushEXTI);

	initPushNVIC.NVIC_IRQChannel = EXTI0_IRQn;
	initPushNVIC.NVIC_IRQChannelPreemptionPriority = 1;
	initPushNVIC.NVIC_IRQChannelSubPriority = 0;
	initPushNVIC.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&initPushNVIC);
}

void init()
{
	  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOA , ENABLE);
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
		led[0].init(GPIOD, GPIO_Pin_12);
		led[1].init(GPIOD, GPIO_Pin_13);
		led[2].init(GPIOD, GPIO_Pin_14);
		led[3].init(GPIOD, GPIO_Pin_15);
		init_push_button();
}

void midiNoteOffHandler(unsigned char channel, unsigned char key, unsigned char vel)
{
	(void)vel;
	if (channel != MIDI_IN_CHANNEL)
		return ;
	GPIO_WriteBit(GPIOD, GPIO_Pin_12, Bit_RESET);
	organ.noteoff(key);
}


void midiNoteOnHandler(unsigned char channel, unsigned char key, unsigned char vel)
{
	if (vel == 0)
	{
		midiNoteOffHandler(channel, key,vel);
		return ;
	}
	if (channel != MIDI_IN_CHANNEL)
		return ;
	GPIO_WriteBit(GPIOD, GPIO_Pin_12, Bit_SET);
	organ.noteon(key,vel);
}

void midiClockHandler()
{
	static int ticks = 0;

	if (ticks % 6 == 3)
		GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_SET);
	else if (ticks % 6 == 0)
		GPIO_WriteBit(GPIOD, GPIO_Pin_14, Bit_RESET);
	ticks++;
}


void midiControlChangeHandler(unsigned char channel, unsigned char control, unsigned char value)
{
	if (channel == MIDI_IN_CHANNEL)
		midiCCProcessor.BufferizeCC(control, value);
}

void midiPitchbendHandler(uint16_t b)
{
	(void)b;
}

void midiStopHandler(void)
{
	organ.panic();
}

void midiResetHandler(void)
{
	organ.panic();
}



static uint32_t initMidiUSB(void)
{
usbd_midi_init_driver(&MIDI_fops);
  USBD_Init(&USB_OTG_dev,
            USB_OTG_FS_CORE_ID,
            &USR_desc,
            &USBD_MIDI_cb,
            &USR_cb);

  return 0;
}

int main(void)
{
	cs43l22 CS;
	extern Midi MIDI ;

/*
  *  IMPORTANT NOTE!
  *  The symbol VECT_TAB_SRAM needs to be defined when building the project
  *  if code has been located to RAM and interrupts are used. 
  *  Otherwise the interrupt table located in flash will be used.
  *  See also the <system_*.c> file and how the SystemInit() function updates 
  *  SCB->VTOR register.  
  *  E.g.  SCB->VTOR = 0x20000000;  
  */

	systickInit_ms();
	init();
	midiCCProcessor.Init(&organ);
	MIDI.begin(MIDI_IN_CHANNEL);
#ifdef USE_MIDI_USB
	initMidiUSB();
#endif
	CS.init();
	while (1)
	{
		MIDI.read();
		midiCCProcessor.ProcessCC();
		organ.fillBuff();
	}
}
