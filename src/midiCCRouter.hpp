/*
 * midiCCRouter.hpp
 *
 *  Created on: Feb 26, 2022
 *      Author: perro
 */

#ifndef midiCCRouter_HPP_
#define midiCCRouter_HPP_


#include "addit.hpp"
#include "midiControls.h"
class midiCCRouter {
private :
	addit *organ;
	unsigned char ccBuffer[128];
	unsigned char ccCurrentBuffer[128];
public:
	midiCCRouter();
	void Init(addit *almondo);
	void ResetToDefault();
	void BufferizeCC(unsigned char cc, unsigned char value);
	void ProcessCC();
	void routeCC(unsigned char cc, unsigned char val);
	void ProcessChannelModeCC(unsigned char cc, unsigned char value);
	virtual ~midiCCRouter();
};

#endif /* midiCCRouter_H_ */
