# AlmondO

AlmondO (originally Almond V0.\<some random digits\> then Almond0 then "Oh AlmondO sounds so nice and friendly") is a tonewheel organ emulation running on a stm32f4 discovery board. (with only SPL libraries, no HAL)

[[_TOC_]]

## Wheeeeeels
The sound engine of AlmondO is made of 109 "wheels" (1 suboctave, 5 octaves plus one note and 3 more octaves for high harmonics, so 12 + 61 + 36 = 109, youpi, I'm so a math wizard 🧙‍♀️ 🧙‍♂️). 

As a wheel is merely a sine generator the problem is :

<br> <b> "How the hell can we run 109 floating point sine generators at 48KHz on a cortex m4 MCU running at 168 MHz and without builtin sine?" </b>

<br> <b>SPOILER ALERT</b> 

We can't..     but here's a shrimp 🦐<br>

But as you may have guessed a shrimp doesn't help in this situation (BTW this code is completly shrimpfree) and I prefered to use some Look Up Tables to deal with the sines issue.

So we have one LUT for each note of the suboctave (not enough memory for 109 LUT) and 109 pointers running through these LUT at various speeds (octave -> speed X 2). 

(Basic LUT won't work neither, but since we're not accessing randomly the tables we can optimize the process to make it fast enough.)

Same explanation without pointers : It's just like the wheel is static but the magnetic head is turning around, and we have plenty of heads around the same wheel and not so many wheels... Easy, isn't it? 👌

At this point we have a splendid screaming machine (all sines, full amplitude and a dying DAC 🙉), so the next step is to choose which sines we want to hear.




## Drawbars

Sines are great but sines are also very poor, just one exact harmonic. So tonewheel organs mix various wheels (harmonics) for each note and we can control the amount of each harmonic with drawbars.
AlmondO has 9 drawbars for 9 harmonics (does it sound familiar?  https://en.wikipedia.org/wiki/Hammond_organ). Each drawbar has 128 positions (MIDI limited).

 <figure>
  <img src="https://gitlab.com/lperron/files_for_other_projects/raw/master/hammonddrawbars.gif" alt="Drawbars" style="width:100%">
  <figcaption>Harmonics and Drawbars</figcaption>
</figure>


So AlmondO just has to mix together the nine harmonics of each note and mix each note played to get an ears friendly sound.

## Nice Additions

* <b>Harmonic leakage</b> : in real tonewheel organs each wheel is packed in a bin with another one (four octaves higher or lower, question of perspective), so a little amount of the signal of the "bin-mate wheel" will leak to the magnetic head of the other one. It sounds like a bad thing but it's not, so Almondo can simulate this, and we can control the amount of leakage.
* <b>Vibrato</b> : well, a tonewheel organ without a vibrato isn't really a tonewheel organ. So AlmondO has a time varying delay vibrato buit in. (AND ONE MORE SINE ~)
* <b>Glitchy Disto</b> : not a real distortion but it can make some nice sounds, This effect is based on signed/unsigned cast (Yep it was originally a bug..). It generates too much high harmonics so a LPF can enhance the sound but if used with soft ADSR it can be pretty organic.
* <b>ADSR</b> : Yep, organs don't have ADSR, but I like pads too much. Plus it sounds so greaaat 🤩

## Controls

AlmondO is MIDI controlled, all parameters can be edited in the  midiControls.h file.

| Contol | Function | Default Value |
| :---   | ----- |---|
| USER_MIDI_IN_CHANNEL | MIDI in channel | 1 |
| MIDISUST | Sustain pedal | 64 |
| MIDISOST | Sostenuto pedal | 66 |
| CCESTR | Gltichy Distortion Amount | 28 |
| CCENVLVL | ADSR Level <br> main amp, volume | 86 |
| CCENVA | ADSR Attack Time  | 87 |
| CCENVD | ADSR Decay Time  | 90 |
| CCENVS | ADSR DSustain Level  | 77 |
| CCENVR | ADSR Release Time  | 78 |
| CCHAR[1-9] | Nth Drawbar Level <br> Control the amount of the Nth harmonic of each note | 1 : 106 <br> 2 : 109 <br> 3 : 110 <br> 4 : 111 <br> 5 : 112 <br> 6 : 115 <br> 7 : 118 <br> 8 : 119 <br> 9 :   75 |
| CCLEAKS | Leaks Amount [0-0.2]  | 29 |
| CCMOD | Modulation Wheel <br> Vibrato Level | 1 |
| CCSWITCH_LEAKS | value >= 64 :   leaks on <br> value < 64 :   leaks off | 31 |

The on/off switch of the harmonic leakage can also be triggered by the user button on the discovery board. 


## Polyphony

Almondo can play 18 notes at the same time without the leakage and 12 with it. It's sufficient for classic organ with only one keyboard but it can be short with long ADSR release and harmonic leakage.

## TODO

* <b>Vibrato enhancement : </b>Like varying speed and amount of vibrato at the same time and it would also be great if the vibrato takes time to reach its speed (basically I want my Yamaha Motif vibrato).
* <s><b>Full control :</b> MIDI for the harmonic leakage switch, just forgot to do it. </s>
* <b>Polyphony++ : </b><s>At least to reach 12 voices with leakage,</s> the best would be 16/24 but it seems too far...
* <b>Hardware : </b>AlmondO has a lot of controls; even with my mopho keyboard and all its knobs Midi control is not so instinctive. So custom hardare would be great (furthermore we will have 12bits precision). But here's the problem : I need to find some nice and cheap slide potentiometers on Aliexpress.. I can't make up my mind...
* <s><b>USB MIDI : </b>Yeah it would be awesome, but USB is huge, USB scares me 👻  Maybe another day.</s> DONE, for the old USBless version @ [OldNoUSB](../../tree/OldNoUSB/)
* <b>MIDILib : </b> I often work with MIDI and it will nice to have a ready to use MIDI library for future stm32 projects. Almondo's MIDI implementation is pretty clean so it can be a good starting point!
* <b>Discovery FREE : </b> AlmondO only uses one pin (Midi in), 3 leds, the cs43l22 DAC/amp and the user push button of the Discovery board. So designing my own circuit would be straightforward, but not the LQFP MCU soldering..


